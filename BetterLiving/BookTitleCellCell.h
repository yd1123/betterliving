//
//  BookTitleCellCell.h
//  BetterLiving
//
//  Created by Edwin on 8/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTitleCellCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *Cover;
@property (weak, nonatomic) IBOutlet UILabel *BookTitle;
@property (weak, nonatomic) IBOutlet UILabel *BookAuthor;

- (void)setHighlightedSated:(BOOL)highlighted;

@end
