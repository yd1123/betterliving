//
//  main.m
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
