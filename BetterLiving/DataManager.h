//
//  DataManager.h
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

typedef void (^requestCompletionBlock) (NSString* aResponse, NSError* anError);
typedef void (^requestListCompletionBlock) (NSArray* aListOfResponses, NSError* anError);

#define author @"author"
#define imageURI @"imageURI"

+ (DataManager*) ShareInstance;

- (void) saveData;

- (void) revokeTime;

- (void) todaysQuote1WithCompletionBlock: (requestCompletionBlock) aBlock ;

- (void) todaysQuote2WithCompletionBlock: (requestCompletionBlock) aBlock ;


- (void) todaysQuote1BookNameWithCompletionBlock: (requestCompletionBlock) aBlock ;

- (void) todaysQuote2BookNameWithCompletionBlock: (requestCompletionBlock) aBlock ;


- (void) todaysQuote1AlbumNameWithCompletionBlock: (requestCompletionBlock) aBlock ;

- (void) todaysQuote2AlbumNameWithCompletionBlock: (requestCompletionBlock) aBlock ;

- (void) albumListWithCompletionBlock: (requestListCompletionBlock) aBlock;

- (void) bookListWithCompletionBlock: (requestListCompletionBlock) aBlock;

- (void) setBookName:(NSString*) aBookName;

- (NSDictionary*) getBookInfoForTitle: (NSString*) bookTitle;

@end
