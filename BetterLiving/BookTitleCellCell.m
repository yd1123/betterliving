//
//  BookTitleCellCell.m
//  BetterLiving
//
//  Created by Edwin on 8/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "BookTitleCellCell.h"

@implementation BookTitleCellCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlightedSated:(BOOL)highlighted{
    if (highlighted) {
        [self.BookAuthor setTextColor:[UIColor brownColor]];
        [self.BookTitle setTextColor:[UIColor brownColor]];
    }else{
        [self.BookAuthor setTextColor:[UIColor blackColor]];
        [self.BookTitle setTextColor:[UIColor blackColor]];
    }
}


@end
