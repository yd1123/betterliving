//
//  ContainerViewController.m
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "ContainerViewController.h"

@interface ContainerViewController ()

@end

@implementation ContainerViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIStoryboard* iPhoneStoryBoard = [self storyiPhoneStoryBoard];
    [self setLeftController:[iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"MenuViewController"]];
    [self setCenterController:[iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"CenterViewController"]];
    [self setRightController:[iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"RightViewController"]];
    self.rightSize = 20;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (UIStoryboard* ) storyiPhoneStoryBoard {
    return [UIStoryboard storyboardWithName:@"iPhoneStoryboard" bundle:nil];
}

@end
