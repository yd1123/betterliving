//
//  Quotes.m
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "Quotes.h"
#import "SystemConfig.h"


@implementation Quotes

@dynamic album;
@dynamic book;
@dynamic frequency;
@dynamic quote;
@dynamic newRelationship;
@dynamic newRelationship1;

@end
