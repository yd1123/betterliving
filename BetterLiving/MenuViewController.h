//
//  MenuViewController.h
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *menuTable;

@end
