//
//  DataManager.m
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "DataManager.h"
#import "SystemConfig.h"
#import "Quotes.h"

@interface DataManager ()

#define version [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]



/*
 
 +root
 |
 + -- <book name>
 |  |
 |  + -- <album name>
 |  |  |
 |  |  + -- <contain>
 |  |     |
 |  |     |
 |  |     ...
 |  + -- <album name>
 |  |
 |  ...
 + -- <book name>
 |
 ...
 */


typedef void (^systemCompletionBlock) ();

@property (nonatomic, strong) NSDictionary *containDictionary;

@property (nonatomic, strong) UIManagedDocument *dbDocument;


@property (nonatomic, strong) NSMutableArray* actionBlocksQueeue;

@property (nonatomic, strong) NSDictionary* bookInfo;

@end


@implementation DataManager

static DataManager* _sharedInstance = NULL;




- (id) init {
    if (self = [super init]) {
        [self setupDocument];
    }
    return self;
}


- (SystemConfig*) updateContent {
    SystemConfig* setting = [self configuration];
    if (!setting.dataUpToDate.boolValue) {
        [self updateFromLocalFile:[[NSBundle mainBundle] pathForResource:@"QuotesBase" ofType:@"plist"]];
        setting.dataUpToDate = [NSNumber numberWithBool:true];
    }
    if ([self doesTodayQuoteNeedToRefresh:setting]) {
        [self updateTodaysQuote:setting];
        setting.todayQuoteLastUpdate = [NSDate dateWithTimeIntervalSinceNow:0];
    }
    [self executeDeferedActions];
    return setting;
}

- (bool) doesTodayQuoteNeedToRefresh: (SystemConfig*) aConfig{
    NSDate *LastUpdateTime = aConfig.todayQuoteLastUpdate;
    if (!LastUpdateTime) {
        aConfig.todayQuoteLastUpdate = [NSDate dateWithTimeIntervalSince1970:0];
        LastUpdateTime = aConfig.todayQuoteLastUpdate;
    }
    NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:LastUpdateTime];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:yesterday];
    bool returnValue = !([comp1 day]   == [comp2 day] &&
                            [comp1 month] == [comp2 month] &&
                            [comp1 year]  == [comp2 year]);
    
    return returnValue;
}


- (void) updateFromLocalFile: (NSString*) aFilePath {
    NSDictionary* aUpDate = [NSDictionary dictionaryWithContentsOfFile:aFilePath];
    [aUpDate enumerateKeysAndObjectsUsingBlock:^(id aBookKey, id obj, BOOL *stop) {
        if ([[aBookKey class] isSubclassOfClass:[NSString class]] &&
            [[obj class] isSubclassOfClass:[NSDictionary class]]) {
            [((NSDictionary*) obj) enumerateKeysAndObjectsUsingBlock:^(id anAlbumKey, id obj, BOOL *stop) {
                if ([[anAlbumKey class] isSubclassOfClass:[NSString class]] &&
                    [[obj class] isSubclassOfClass:[NSArray class]]) {
                    [self insertQuotesData:obj
                                 withAblum:anAlbumKey
                               andBookName:aBookKey
                             forEntityName:NSStringFromClass([Quotes class])];
                }
            }];
        }
    }];
}



#pragma mark - Setup coredata
- (void) setupDocument{
    NSURL* dburl = [self applicationDocumentsDirectory];
    dburl = [self databaseDocumentUrl:dburl];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // this going to both setup and use the document.
        self.dbDocument = [[UIManagedDocument alloc] initWithFileURL:dburl];
        [self usejobmineDocument];
    });
}

- (void) usejobmineDocument{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self.dbDocument.fileURL path]]) {
        [self.dbDocument saveToURL:self.dbDocument.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if (success) {
                [self updateContent];
            }
        }];
    }else if (self.dbDocument.documentState == UIDocumentStateClosed){
        [self.dbDocument openWithCompletionHandler:^(BOOL success) {
            if (success) {
                [self updateContent];
            }
        }];
    }else if (self.dbDocument.documentState == UIDocumentStateNormal){
        [self.dbDocument openWithCompletionHandler:^(BOOL success) {
            if (success) {
                [self updateContent];
            }
        }];
    }else{
        [NSException raise:@"document Error UNknow doucment State: " format:@"%i", self.dbDocument.documentState];
    }
}



#pragma mark - Use coredata


- (SystemConfig*) configuration {
    
    NSFetchRequest* cfgFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([SystemConfig class])];
    cfgFetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"dataUpToDate" ascending:YES]];
    
    NSError* error = nil;
    NSArray *qResult = [self.dbDocument.managedObjectContext executeFetchRequest:cfgFetchRequest error:&error];
    
    if (qResult.count == 1) {
        return [qResult objectAtIndex:0];
    } else if (qResult.count == 0){
        
        return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([SystemConfig class])
                                             inManagedObjectContext:self.dbDocument.managedObjectContext];
    }else{
        [NSException raise:[NSString stringWithFormat:@"WTF: %d", qResult.count] format:@""];
        return nil;
    }
}


- (void) insertQuotesData: (NSArray*) anArray withAblum: (NSString*) anAlbum andBookName:(NSString*) abookName forEntityName: (NSString*) anEntityName{
    [anArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Quotes* aQuote = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Quotes class])
                                      inManagedObjectContext:self.dbDocument.managedObjectContext];
        aQuote.book = abookName;
        aQuote.album = anAlbum;
        aQuote.quote = obj;
    }];
}

- (void) updateTodaysQuote: (SystemConfig*) aConfig{
    
    NSString* quoteBook = [self todayBookName:aConfig];
    
    // Update number Quote number 2
    NSString* quoteAlbum1Name = [self todayAlbum1Name:aConfig];
    NSString* quoteAlbum2Name = [self todayAlbum2Name:aConfig];
    
    NSFetchRequest* aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Quotes class])];
    aFetchRequest.predicate = [NSPredicate predicateWithFormat:@"(book MATCHES[cd] %@) AND (album MATCHES[cd] %@)", quoteBook
                               , quoteAlbum2Name];
    aFetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"quote" ascending:YES]];
    
    NSError* error = nil;
    NSArray *qResult = [self.dbDocument.managedObjectContext executeFetchRequest:aFetchRequest error:&error];
    
    NSUInteger randomQuoteNumber = [self randomWithLowerBound:0 andUpperBound:qResult.count];
    if (!error) {
        aConfig.todayEntry2 = qResult[randomQuoteNumber];
        NSUInteger frequency = ((Quotes*)qResult[randomQuoteNumber]).frequency.unsignedIntegerValue;
        ((Quotes*)qResult[randomQuoteNumber]).frequency = [NSNumber numberWithUnsignedInt:(frequency+1)];
    }
    
    // Update number Quote number 1
    
    aFetchRequest.predicate = [NSPredicate predicateWithFormat:@"(book MATCHES[cd] %@) AND (album BEGINSWITH[cd] %@)",
                               quoteBook,
                               quoteAlbum1Name];
    
    qResult = [self.dbDocument.managedObjectContext executeFetchRequest:aFetchRequest error:&error];
    randomQuoteNumber = [self randomWithLowerBound:0 andUpperBound:qResult.count];
    if (!error) {
        aConfig.todayEntry = qResult[randomQuoteNumber];
        NSUInteger frequency = ((Quotes*)qResult[randomQuoteNumber]).frequency.unsignedIntegerValue;
        ((Quotes*)qResult[randomQuoteNumber]).frequency = [NSNumber numberWithUnsignedInt:(frequency+1)];
    }
}




- (NSString*) todayBookName: (SystemConfig*) aConfig {
    if (aConfig.todayQuoteBook == nil){
        aConfig.todayQuoteBook = @"How to Stop Worrying and Start Living";
    }
    return aConfig.todayQuoteBook;
}

- (NSString*) todayBookName2: (SystemConfig*) aConfig {
    if (aConfig.todayQuoteBook2 == nil){
        aConfig.todayQuoteBook2 = @"How to Stop Worrying and Start Living";
    }
    return aConfig.todayQuoteBook;
}

- (NSString*) todayAlbum1Name: (SystemConfig*) aConfig {
    if (aConfig.todayQuoteAblum == nil){
        aConfig.todayQuoteAblum = @"Principle";
    }
    return aConfig.todayQuoteAblum;
}

- (NSString*) todayAlbum2Name: (SystemConfig*) aConfig {
    if (aConfig.todayQuoteAblum2 == nil){
        aConfig.todayQuoteAblum2 = @"Quotes";
    }
    return aConfig.todayQuoteAblum2;
}

- (NSArray*) fetchQuoteList:(NSString*)anAttribute{
    NSFetchRequest* aFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Quotes class]) inManagedObjectContext:self.dbDocument.managedObjectContext];
    [aFetchRequest setEntity:entity];
    [aFetchRequest setResultType:NSDictionaryResultType];
    [aFetchRequest setReturnsDistinctResults:YES];
    [aFetchRequest setPropertiesToFetch:@[anAttribute]];
    aFetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:anAttribute ascending:YES]];
    
    NSError* error = nil;
    NSArray *qResult = [self.dbDocument.managedObjectContext executeFetchRequest:aFetchRequest error:&error];
    return qResult;
}



#pragma mark - Utility

- (NSMutableArray*) actionBlocksQueeue{
    if (!_actionBlocksQueeue) {
        _actionBlocksQueeue = [NSMutableArray new];
    }
    return _actionBlocksQueeue;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)databaseDocumentUrl:(NSURL *)anDburl {
    NSString* documentName = [[NSString alloc] initWithFormat:@"BetterLiving%@", version];
    return [anDburl URLByAppendingPathComponent:documentName];
}


- (NSUInteger) randomWithLowerBound:(NSUInteger) aUpperBound andUpperBound: (NSUInteger) aLowerBound {
    int lowerBound = aUpperBound;
    int upperBound = aLowerBound;
    return lowerBound + arc4random() % (upperBound - lowerBound);
}


- (void) pushActionQueeue: (systemCompletionBlock) aBlock{
    @synchronized(self){
        [self.actionBlocksQueeue addObject:aBlock];
    }
}

- (systemCompletionBlock) popActionQueeue{
    @synchronized(self){        
        if (self.actionBlocksQueeue.count > 0) {
            systemCompletionBlock result = self.actionBlocksQueeue.lastObject;
            [self.actionBlocksQueeue removeLastObject];
            return result;
        }
    }
    return nil;
}


- (bool) isDataReady{
    return self.dbDocument != nil && self.dbDocument.documentState == UIDocumentStateNormal;
}

- (void) executeDeferedActions{
    while (self.actionBlocksQueeue.count > 0) {
        systemCompletionBlock block = [self popActionQueeue];
        dispatch_async(dispatch_get_main_queue(), ^{
            block();
        });
    }
}


#pragma mark - getter & setter

- (NSDictionary *)bookInfo{
    if (!_bookInfo) {
        _bookInfo = @{@"How to Win Friends and Influence People": @{author: @"Dale Carnegie",
                                                                    imageURI: @"winFriend.jpg"},
                      @"How to Stop Worrying and Start Living": @{author: @"Dale Carnegie",
                                                                  imageURI: @"startLiving.jpg"}
                      };
    }
    return _bookInfo;
}


#pragma mark - Public Api
+ (DataManager*) ShareInstance {
    if (!_sharedInstance) {
        _sharedInstance = [[DataManager alloc] init];
    }
    return _sharedInstance;
}




- (NSDictionary*) getBookInfoForTitle: (NSString*) bookTitle{
    NSDictionary* result = self.bookInfo[bookTitle];
    if (!result) {
        result = @{author: @"Unknown", imageURI: @"book"};
    }
    return result;
}


- (void) saveData{
    if ([self isDataReady]) {
        [self.dbDocument savePresentedItemChangesWithCompletionHandler:^(NSError *errorOrNil) {
            NSLog(@"%@", errorOrNil);
        }];
    }
}

- (void) todaysQuote1WithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry.quote, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}

- (void) todaysQuote2WithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry2.quote, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}


- (void) todaysQuote1BookNameWithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry.book, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}

- (void) todaysQuote2BookNameWithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry2.book, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}


- (void) todaysQuote1AlbumNameWithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry.album, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}

- (void) todaysQuote2AlbumNameWithCompletionBlock: (requestCompletionBlock) aBlock {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* aConfig = [self updateContent];
        aBlock(aConfig.todayEntry2.album, nil);
        
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}


- (void) albumListWithCompletionBlock: (requestListCompletionBlock) aBlock {
    
    systemCompletionBlock deferedAction = ^{
        NSArray* aListOfAlbum = [self fetchQuoteList:@"album"];
        NSMutableArray* resultArray = [NSMutableArray new];
        [aListOfAlbum enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj class] isSubclassOfClass:[NSDictionary class]]) {
                [resultArray addObject:[((NSDictionary*)obj) objectForKey:@"album"]];
            }
        }];
        aBlock(resultArray, nil);
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}


- (void) bookListWithCompletionBlock: (requestListCompletionBlock) aBlock {
    
    systemCompletionBlock deferedAction = ^{
        NSArray* aListOfAlbum = [self fetchQuoteList:@"book"];
        NSMutableArray* resultArray = [NSMutableArray new];
        [aListOfAlbum enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj class] isSubclassOfClass:[NSDictionary class]]) {
                [resultArray addObject:[((NSDictionary*)obj) objectForKey:@"book"]];
            }
        }];
        aBlock(resultArray, nil);
    };
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}


- (void) setBookName:(NSString*) aBookName{
    systemCompletionBlock deferedAction = ^{
        SystemConfig* setting = [self configuration];
        setting.todayQuoteBook = aBookName;
        setting.todayQuoteBook2 = aBookName;
    };
    
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
}

- (void) revokeTime {
    systemCompletionBlock deferedAction = ^{
        SystemConfig* setting = [self configuration];
        setting.todayQuoteLastUpdate = [NSDate dateWithTimeIntervalSince1970:0];
    };
    
    if ([self isDataReady]) {
        deferedAction();
    }else{
        [self pushActionQueeue:deferedAction];
    }
    
}



@end
