//
//  SystemConfig.h
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Quotes;

@interface SystemConfig : NSManagedObject

@property (nonatomic, retain) NSNumber * dataUpToDate;
@property (nonatomic, retain) NSString * todayQuoteAblum;
@property (nonatomic, retain) NSString * todayQuoteBook;
@property (nonatomic, retain) NSDate * todayQuoteLastUpdate;
@property (nonatomic, retain) NSString * todayQuoteBook2;
@property (nonatomic, retain) NSString * todayQuoteAblum2;
@property (nonatomic, retain) Quotes *todayEntry;
@property (nonatomic, retain) Quotes *todayEntry2;

@end
