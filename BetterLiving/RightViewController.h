//
//  RightViewController.h
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *quoteBody;

@end
