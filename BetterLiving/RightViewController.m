//
//  RightViewController.m
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "RightViewController.h"
#import "DataManager.h"

@interface RightViewController ()

@end

@implementation RightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [[DataManager ShareInstance] todaysQuote2WithCompletionBlock:^(NSString *aResponse, NSError *anError) {
        if (anError) {
            [self.quoteBody setText:anError.description];
        }else{
            [self.quoteBody setText:aResponse];
        }
    }];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setQuoteBody:nil];
    [super viewDidUnload];
}
@end
