//
//  ConterViewController.m
//  BetterLiving
//
//  Created by Edwin on 7/20/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "CenterViewController.h"
#import "IIViewDeckController.h"
#import "DataManager.h"

@interface CenterViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageBelowText;
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UITextView *textarea;

@end

@implementation CenterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.viewDeckController previewBounceView:IIViewDeckRightSide];
    [[DataManager ShareInstance] todaysQuote1WithCompletionBlock:^(NSString *aResponse, NSError *anError) {
        if (anError) {
            [self.quoteBody setText:anError.description];
        }else{
            [self.quoteBody setText:aResponse];
        }
    }];
    
    [[DataManager ShareInstance] todaysQuote1AlbumNameWithCompletionBlock:^(NSString *aResponse, NSError *anError) {
        
        if (anError) {
            [self.quoteAlbum setText:anError.description];
        }else{
            [self.quoteAlbum setText:aResponse];
        }
    }];
    
    [self updateView];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setQuoteBody:nil];
    [self setQuoteAlbum:nil];
    [super viewDidUnload];
}



- (void)updateView {
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.1f animations:^{
        CGFloat diff = 5.0f;
        CGFloat yTitle = self.imageBelowText.frame.origin.y + self.imageBelowText.frame.size.height + diff;
        CGFloat yBody = yTitle + self.titleTextLabel.frame.size.height+ diff;
        
        CGRect title = self.titleTextLabel.frame;
        title.origin.y = yTitle;
        self.titleTextLabel.frame = title;
        
        CGRect body = self.textarea.frame;
        body.origin.y = yBody;
        body.size.height = self.view.frame.size.height - yBody - diff;
        self.textarea.frame = body;
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self updateView];
    
}





@end
