//
//  MenuViewController.m
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "MenuViewController.h"
#import "DataManager.h"
#import "IIViewDeckController.h"
#import "BookTitleCellCell.h"
#import "SVProgressHUD.h"

@interface MenuViewController ()

@property (strong, nonatomic) __block NSArray* displayData;
@property (strong, nonatomic) __block NSString* todayBook;

@end

@implementation MenuViewController

- (void)setup
{
    
    [[DataManager ShareInstance] todaysQuote1BookNameWithCompletionBlock:^(NSString *aResponse, NSError *anError) {
        self.todayBook = aResponse;
    }];
    
    [[DataManager ShareInstance] bookListWithCompletionBlock:^(NSArray *aListOfResponses, NSError *anError) {
        [self setDisplayData:aListOfResponses];
        [self.menuTable reloadData];
    }];
    
}

- (void)awakeFromNib{
    [super awakeFromNib];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setup];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - @protocol UITableViewDataSource<NSObject>


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.displayData) {
        return self.displayData.count;
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.displayData) {
        UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"BookTitleCellCell"];
        NSString* cellText = self.displayData[indexPath.row];
        
        if ([aCell isKindOfClass:[BookTitleCellCell class]]) {
            BookTitleCellCell* aBookTitleCellCell = (BookTitleCellCell*) aCell;
            NSDictionary * moreinfo = [[DataManager ShareInstance] getBookInfoForTitle:cellText];
            aBookTitleCellCell.BookTitle.text = cellText;
            aBookTitleCellCell.BookAuthor.text = moreinfo[author];
            aBookTitleCellCell.Cover.image = [UIImage imageNamed:moreinfo[imageURI]];
            [aBookTitleCellCell setHighlightedSated:[self.todayBook isEqualToString:cellText]];
            
        }else{            
            aCell.textLabel.text = cellText;
            if ([self.todayBook isEqualToString:cellText]) {
                [aCell.textLabel setTextColor:[UIColor blueColor]];
            }
        }
        
        return aCell;
    }else{
        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
}


#pragma mark - @protocol UITableViewDelegate<NSObject, UIScrollViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.displayData) {
        [[DataManager ShareInstance] setBookName:self.displayData[indexPath.row]];
//        [[DataManager ShareInstance] revokeTime];

        [SVProgressHUD showSuccessWithStatus:@"This will take effect tomorrow"];
        [self.viewDeckController closeLeftViewAnimated:YES];
        [self setup];
    }
}



- (void)viewDidUnload {
    [self setMenuTable:nil];
    [super viewDidUnload];
}
@end
