//
//  SystemConfig.m
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import "SystemConfig.h"
#import "Quotes.h"


@implementation SystemConfig

@dynamic dataUpToDate;
@dynamic todayQuoteAblum;
@dynamic todayQuoteBook;
@dynamic todayQuoteLastUpdate;
@dynamic todayQuoteBook2;
@dynamic todayQuoteAblum2;
@dynamic todayEntry;
@dynamic todayEntry2;

@end
