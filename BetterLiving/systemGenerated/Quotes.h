//
//  Quotes.h
//  BetterLiving
//
//  Created by Edwin on 7/21/13.
//  Copyright (c) 2013 Edwin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SystemConfig;

@interface Quotes : NSManagedObject

@property (nonatomic, retain) NSString * album;
@property (nonatomic, retain) NSString * book;
@property (nonatomic, retain) NSNumber * frequency;
@property (nonatomic, retain) NSString * quote;
@property (nonatomic, retain) SystemConfig *newRelationship;
@property (nonatomic, retain) SystemConfig *newRelationship1;

@end
